document.addEventListener("DOMContentLoaded", function () {
  loadBooks();
});

function loadBooks() {
  const unfinishedBooks = document.getElementById("unfinished-books");
  const finishedBooks = document.getElementById("finished-books");

  const books = getBooksFromLocalStorage();

  unfinishedBooks.innerHTML = "";
  finishedBooks.innerHTML = "";

  books.forEach((book) => {
    const li = document.createElement("li");
    li.innerHTML = `${book.title} - ${book.author} (${book.year}) <button class="btn-warning" onclick="moveBook('${book.id}')">Pindah</button> <button class="btn-danger" onclick="deleteBook('${book.id}')">Hapus</button>`;
    
    if (book.isComplete) {
      finishedBooks.appendChild(li);
    } else {
      unfinishedBooks.appendChild(li);
    }
  });
}

function addBook() {
  const title = document.getElementById("title").value;
  const author = document.getElementById("author").value;
  const year = parseInt(document.getElementById("year").value);
  const isComplete = document.getElementById("isComplete").checked;
  const id = +new Date();

  const newBook = {
    id,
    title,
    author,
    year,
    isComplete,
  };

  const books = getBooksFromLocalStorage();
  books.push(newBook);
  saveBooksToLocalStorage(books);

  loadBooks();
  clearForm();
}

function moveBook(bookId) {
  const books = getBooksFromLocalStorage();
  const bookIndex = books.findIndex((book) => book.id == bookId);

  if (bookIndex !== -1) {
    books[bookIndex].isComplete = !books[bookIndex].isComplete;
    saveBooksToLocalStorage(books);
    loadBooks();
  }
}

function deleteBook(bookId) {
  const books = getBooksFromLocalStorage();
  const filteredBooks = books.filter((book) => book.id != bookId);
  saveBooksToLocalStorage(filteredBooks);
  loadBooks();
}

function getBooksFromLocalStorage() {
  const booksJSON = localStorage.getItem("books");
  return booksJSON ? JSON.parse(booksJSON) : [];
}

function saveBooksToLocalStorage(books) {
  localStorage.setItem("books", JSON.stringify(books));
}

function clearForm() {
  document.getElementById("title").value = "";
  document.getElementById("author").value = "";
  document.getElementById("year").value = "";
  document.getElementById("isComplete").checked = false;
}
